package pl.sda.javaii.thread.zad2;

import java.util.Observable;
import java.util.Observer;

public class AlexaServer implements Observer{

    public AlexaServer() {

    }
    public AbstractSkill parseRequest(Request request){
        String req = request.getRequest();
        req = req.toLowerCase();
        if (req.startsWith("alexa")) {
            req = req.substring(6); // 5 znaków oraz
            if (req.toLowerCase().startsWith("what's the time") ||
                    req.toLowerCase().startsWith("give me the time") ||
                    req.toLowerCase().startsWith("the time") ||
                    req.toLowerCase().startsWith("what time is it")) {
                return new DateTimeSkill();
            }
        }
        return new DontKnowSkill();
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Request){
            Request request = (Request) arg;
        AbstractSkill callback = parseRequest(request);
        request.getDeviceToCall().invoke(callback);
    }}
}
