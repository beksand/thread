package pl.sda.javaii.thread.zad2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        AlexaServer server = new AlexaServer();

        AlexaDevice device = new AlexaDevice();

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.equalsIgnoreCase("register")) { // podłącz internet
                if (device.countObservers() == 0) {
                    device.addObserver(server);
                }
            } else if (line.equalsIgnoreCase("unregister")) { // rozłącz internet
                device.deleteObserver(server);
            } else {
                device.sendRequest(line); // wykonaj zapytanie do alexy
            }
        }
    }
}
