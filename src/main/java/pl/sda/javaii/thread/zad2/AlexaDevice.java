package pl.sda.javaii.thread.zad2;

import java.util.Observable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AlexaDevice extends Observable {
    private static int alexaIds = 0;
    private int id = alexaIds;
    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    public AlexaDevice() {
    }
    public void sendRequest(String requestString) {
        if (requestString.toLowerCase().startsWith("alexa")) {
            Request request = new Request(requestString, this);
            setChanged();
            notifyObservers(request);
        }
    }
    public void invoke(AbstractSkill callback){
        executorService.submit(callback);
    }
}
