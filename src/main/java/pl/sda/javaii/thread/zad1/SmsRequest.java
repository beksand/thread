package pl.sda.javaii.thread.zad1;


public class SmsRequest implements Runnable{

    private ISmsSender station;
    private Message message;

    public SmsRequest(String number, String content, ISmsSender station) {
        this.message = new Message(number, content);
        this.station = station;
    }


    public void run() {
        try {
            System.out.println("rozpoczynam szyfrowania wiadomosti!");
            Thread.sleep(100*message.getContent().length());
            System.out.println("wysylam wiadomosc");
            station.sendSms(message);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
