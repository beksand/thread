package pl.sda.javaii.thread.zad1;

import java.io.StringReader;
import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {

    private String numberPhone;

    public Phone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    public String getNumberPhone() {
        return numberPhone;
    }

    public void setNumberPhone(String numberPhone) {
        this.numberPhone = numberPhone;
    }

    @Override
    public void update(Observable o, Object message) {
        if (message instanceof Message){
            Message request = (Message) message;
            if (request.getNumber().equalsIgnoreCase(getNumberPhone())){
            System.out.println(numberPhone+" otrzymal "+request.getContent());
        }}
    }
}
