package pl.sda.javaii.thread.zad1;

public interface ISmsSender {
    void sendSms(Message request);
}
