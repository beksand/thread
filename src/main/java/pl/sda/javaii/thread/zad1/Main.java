package pl.sda.javaii.thread.zad1;

//Stworzymy aplikację SMS station. Aplikacja ma symulować pracę stacji smsowej.
//        Stwórz klasę SmsStation. zakładamy że stacja smsowa otrzymuje jakieś zlecenie o wysłaniu smsa, które musi się
// wydarzyć poprzez broadcast (stacja jest observable, telefony sa observerami). Ta klasa powinna mieć metodę:
//        void sendSms(String numer, String tresc)
//        która tworzy obiekt typu SmsRequest(Runnable), a następnie przekazuje go do puli wątków (/anten) do wykonania.
// Klasa SmsRequest posiada dwa pola (tresc i numer). W metodzie run() SmsRequest najpierw czeka określoną ilość czasu
// (100ms * ilość_liter_w_smsie), po czym wypisuje komunikat o wysłaniu smsa.
//        Message moze byc runnable. Sms station jest klasą która posiada pulę wątków.
//        Użytkownik z poziomu Main'a wpisuje komendę:
//        1235346457 jakas tresc smsa
//        Po otrzymaniu komendy podziel ją przez split na numer (1 słowo komendy) oraz pozostałą część komendy - to
// będzie treść smsa.

public class Main {
    public static void main(String[] args) {
        SmsStation station = new SmsStation();

        station.addPhone("123");
        station.addPhone("124");
        station.addPhone("125");

        station.sendSmsRequest("123", "Hej Bro!");
        station.sendSmsRequest("124", "Hej Bro!!!!!!!!!!!!!!!!!");
        station.sendSmsRequest("125", "hej");
    }
}
