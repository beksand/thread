package pl.sda.javaii.thread.zad1;

import java.util.Observable;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class SmsStation extends Observable implements ISmsSender{

    private Executor anteny = Executors.newFixedThreadPool(5);


    public void addPhone(String number){
        addObserver(new Phone(number));
    }

    public void sendSmsRequest(String number, String content){
        SmsRequest smsRequest = new SmsRequest(number, content, this);
        anteny.execute(smsRequest);
    }
    public void sendSms(Message request){
        setChanged();
        notifyObservers(request);
    }
}
